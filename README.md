# Küttel Informatik: Webseite für meine Einzelfirma

Meine Einzelfirma Küttel Informatik bietet verschiedene Dienstleistungen an.

Momentan werden lediglich Webseiten für Privatkunden erstellt, jedoch möchte ich mich eventuell in eine Genossenschaft umwandeln, sobald ich die richtigen 7 Leute kenne.

Ich (Moritz Küttel) bin freiberuflicher Free- & Open Source Software-Entwickler und arbeite an technologischen und sozialen Problemen,
arbeite gerne Transparent und
möchte es gerne allen Benutzern zu erlauben einen Computer so verwenden zu können, wie sie das gerne wollen (falls diese überhaupt ein Computer wollen).

## Backstory

## Contributions / Helfer gesucht

* **Hast du einen Fehler gefunden?**  
  Schreibfehler, weitere Inhalte / Bilder:  
  Ideen sind auch willkommen.
  Report a Bug here: [CodeBerg.Org Issue Tracker](https://codeberg.org/mkuettel/kuettel.ch/issues/new)
* *War etwas mühsam zu bewerkstelligen mit der Software?*
  Bitte Schreib mir ein Mail an moritz_kuettel@fastmail.com .
  Gerne können wir auch Telefonieren oder uns privat Treffen,
  um Sachen zu besprechen, damit ich verstehe wie ich am besten helfen kann.

* *Willst du mithelfen beim Entwickeln?* (Developer?)
  Bitte Schreib mir ein Mail an moritz_kuettel@fastmail.com
  oder schau dir mal die Quellcode Repository auf [codeberg.org/mkuettel/kuettel.ch](https://codeberg.org/mkuettel/kuettel.ch)  
  Es handelt sich hier um AGPL? Software. Ich in auch tolerant gegenüber OSI und BSD Lizenzen.


## Konzept / Concept

Bei der Firmenwebseite gibt es einerseits Informationen über die Einzelfirma sowie einen kleinen integrierten Website Builder der vielen verschiedenen Benutzergruppen mit verschiedenen Fähigkeiten gerecht werden soll.

### Features:
 * Single Page Website for Portfolios
 * Host Static Websites
 * Staticcally Generated Websites from
   * Wizard to click through
   * Be able to change things later
   * Maybe Wordpress Blocks in between?
   * Straight up Markdown
   * Everything gets compiled and combined down to markdown
   * Have some Templates and Styles to choose from
 * Have a Mail or Contact Section

Future Expansions:
* More Mail Services
* News and some Interactivity

### Umfang und Abgrenzung/Scope:

Only Web 1.0 First just Front-End generated from Database,
insert Web 2.0 & Web 3.0 later with Backend & AJAX/APIs?

Qualitätfeatures:
 * Gut Bedienbar mit Maus / Handy sowie Tastatur in vielen Browsern.
 * Barrierefreiheit
 * Zugänglich für jeden, egal was sein Skill-Set ist (soweit wie möglich)
 * Verschiedene Benutzerinterfaces für verschiedene Benutzer

* In the end no-one can own an Idea. (First step to wisdom is knowing that you understand nothing).
* Artwork and characters can be owned by People.
* Characters 
* The user, when entering into this wyrd, would still technically have the copyright
  on the of contributed works, but the all the artwork created should be able to be transformed
  and shared on a free basis. This might be a thing for Creative Commons Licences, but it might 
  be difficult to make money this way,


## Technical
